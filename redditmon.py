#!/usr/bin/env python

import os
import sys
import hashlib
import requests
from gntp.notifier import GrowlNotifier
from time import sleep
from random import randint

growl = None
interval = 30


def get_page(url):
    response = requests.get(url)
    if response.status_code == 200:
        # uncomment rest of the line to ONLY monitor selftext of the post.
        return response.json()[0]['data']['children'][0]['data']['selftext']
    else:
        raise Exception('Failed - ' + str(response.status_code))


def update(url, last_hash=None, callback=None):
    data = get_page(url)
    data_hash = hashlib.sha1(data.__str__()).digest()

    if callback is not None and last_hash is not None and last_hash != data_hash:
        callback(url, data)

    return data_hash


def my_callback(url, data):
    if growl is not None:
        growl.notify(
            noteType='Updated',
            title='Updated!',
            description='New updates to thread!',
            icon='http://www.reddit.com/static/spreddit2.gif',
            sticky=True,
            callback=url[:url.find('.json')],
            priority=2,
        )

    print 'Updated!'


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Error: requires url to monitor"
        os.exit(1)
    url = sys.argv[1]

    growl = GrowlNotifier(
        applicationName='Monitor',
        notifications=['Updated'],
    )
    growl.register()

    last_hash = None
    while True:
        try:
            last_hash = update(url, last_hash=last_hash, callback=my_callback)
        except Exception as e:
            print e

        # jitter our updates
        sleep(randint(interval-5, interval+5))
